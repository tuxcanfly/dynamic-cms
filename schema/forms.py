from django import forms
from django.utils.translation import ugettext_lazy as _
from django.forms.models import inlineformset_factory

from crispy_forms.helper import FormHelper
from crispy_forms.layout import (Fieldset, Layout, Submit, Reset,
        ButtonHolder, MultiField)

from schema.models import Model, Field


class ModelForm(forms.ModelForm):

    class Meta:
        model = Model

    class Media:
        js = ('/static/js/jquery.formset.js', )

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
                Fieldset(
                    _('Add a new Model'),
                    'name',
                    'child',),
                )

        super(ModelForm, self).__init__(*args, **kwargs)

FieldFormset = inlineformset_factory(Model, Field, extra=1)
