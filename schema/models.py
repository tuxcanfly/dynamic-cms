from django.db import models

from schema.mixins import AutoSlugMixin


KIND_CHOICES = (
    ('STRING', 'string'),
    ('DATE', 'date'),
    ('INTEGER', 'integer'),
    ('DROPDOWN', ' dropdown'),
)


class Model(AutoSlugMixin, models.Model):
    name = models.CharField(max_length=100, unique=True)
    child = models.ForeignKey('Model', null=True, blank=True)
    slug = models.SlugField(editable=False)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.update_slug(commit=False)
        return super(Model, self).save(*args, **kwargs)


class Field(models.Model):
    name = models.CharField(max_length=100)
    kind = models.CharField(max_length=10, choices=KIND_CHOICES)
    model = models.ForeignKey(Model, related_name="fields")
    choices = models.CharField(max_length=200, null=True, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        unique_together = ('name', 'model')


class Objekt(models.Model):
    model = models.ForeignKey(Model, related_name="instances")
    parent = models.ForeignKey('Objekt', null=True, blank=True, related_name="related")

    def __unicode__(self):
        if self.attrs.count():
            return '-'.join(list(self.attrs.values_list('value', flat=True)))
        else:
            return "instance of %s" % (self.model.name)


class KeyValue(models.Model):
    key = models.CharField(max_length=100)
    value = models.TextField()
    objekt = models.ForeignKey(Objekt, related_name="attrs")

    def __unicode__(self):
        return self.key
