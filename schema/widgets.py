from django import forms

FIELD_WIDGETS = {
    'STRING': forms.CharField(max_length=50),
    'DATE': forms.DateField(),
    'INTEGER': forms.IntegerField(),
}
