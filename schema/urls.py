from django.conf.urls import patterns, url


urlpatterns = patterns('schema.views',
    url(r'^$', 'home', name='home'),
    url(r'^model/add/$', 'model_create', name='model_create'),
    url(r'^object/add/(?P<slug>[\w-]+)/$', 'object_create', name='object_create'),
    url(r'^object/list/(?P<slug>[\w-]+)/$', 'object_list', name='object_list'),
)
