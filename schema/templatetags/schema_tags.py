from django import template

register = template.Library()

@register.filter
def filter(qs, model):
    return qs.filter(model=model)
