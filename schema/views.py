from django.views.generic.edit import CreateView, FormView
from django.views.generic.list import ListView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import get_object_or_404
from django.utils.datastructures import SortedDict
from django.template.defaultfilters import slugify
from django.contrib import messages
from django.forms import BaseForm
from django import forms

from crispy_forms.helper import FormHelper

from schema.forms import ModelForm, FieldFormset
from schema.models import Model, Objekt, KeyValue
from schema.mixins import InlineFormsetMixin
from schema.widgets import FIELD_WIDGETS


class HomepageView(ListView):
    model = Model
    template_name = "schema/home.html"
    context_object_name = "models"


class ObjectListView(ListView):
    template_name = "schema/object_list.html"
    context_object_name = "objects"

    def get_queryset(self):
        model = get_object_or_404(Model, slug=self.kwargs['slug'])
        return Objekt.objects.filter(model=model)

    def get_context_data(self, **kwargs):
        context = super(ObjectListView, self).get_context_data(**kwargs)
        model = get_object_or_404(Model, slug=self.kwargs['slug'])
        context['model'] = model
        return context


class ModelCreateView(InlineFormsetMixin, CreateView):
    form_class = ModelForm
    model = Model
    inline_formsets = {
        'field_formset': FieldFormset
    }
    success_url = reverse_lazy("home")


class ObjectCreateView(FormView):
    success_url = reverse_lazy("home")
    template_name = 'schema/object_form.html'

    def get_form_class(self):
        model = get_object_or_404(Model, slug=self.kwargs['slug'])
        fields = SortedDict()
        for field in model.fields.all():
            if field.kind == 'DROPDOWN':
                fields[field.name] = forms.ChoiceField(choices=(
                    (x, x) for x in field.choices.split(',')))
            else:
                fields[field.name] = FIELD_WIDGETS[field.kind]
        if model.child_id:
            fields[model.child.name] = forms.ModelChoiceField(
                    queryset=Objekt.objects.filter(model=model.child))
        BaseForm.helper = FormHelper()
        BaseForm.helper.form_tag = False
        return type('ObjectForm', (BaseForm,), { 'base_fields':
            fields
        })

    def form_valid(self, form):
        model = get_object_or_404(Model, slug=self.kwargs['slug'])
        objekt = Objekt.objects.create(model=model)
        kwargs = form.cleaned_data.copy()
        if model.child_id:
            objekt.parent = kwargs.pop(model.child.name)
            objekt.save()
        for k, v in kwargs.iteritems():
            k = slugify(k)
            attr = KeyValue.objects.create(key=k, value=v,
                    objekt=objekt)
        messages.success(self.request, 'New %s created' % (model.name))
        return super(ObjectCreateView, self).form_valid(form)


home = HomepageView.as_view()
model_create = ModelCreateView.as_view()
object_create = ObjectCreateView.as_view()
object_list = ObjectListView.as_view()
