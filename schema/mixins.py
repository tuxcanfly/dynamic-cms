from django.http import HttpResponseRedirect
from django.core.exceptions import ImproperlyConfigured
from django.template.defaultfilters import slugify


class InlineFormsetMixin(object):
    """
    This mixin allows a user to specify inline formsets that should be rendered
    alongside a Form or ModelForm.

    This mixin should occur before the form mixins in the inheritance list, so
    that the context dictionary they return can be modified by the
    ``get_context_data`` method in this mixin.

    Note also that ``form_valid`` is overridden here and will NOT delegate back
    to the form mixins. The behavior (redirecting to ``self.get_success_url``)
    is preserved, however. Note also that the ModelForm object is saved if and
    only if the original form and the inline formsets are all valid.
    """
    inline_formsets = None

    def form_valid(self, form):
        from operator import methodcaller
        context = self.get_context_data(form=form)
        formsets = map(context.get, self.inline_formsets.keys())
        if all(map(methodcaller('is_valid'), formsets)):
            self.object = form.save()
            for formset in formsets:
                formset.instance = self.object
                formset.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        """ Adds the items from self.inline_formsets to the context dict. """
        import collections
        if not isinstance(self.inline_formsets, dict):
            raise ImproperlyConfigured('%(cls)s.inline_formsets must be a '
                'dict representing the context dict key and the formset '
                'class for each desired inline formset.' % {
                    'cls': self.__class__,
                })
        context = super(InlineFormsetMixin, self).get_context_data(**kwargs)
        for key, formset_cls in self.inline_formsets.items():
            if self.request.POST:
                formset = formset_cls(self.request.POST, instance=self.object)
            else:
                formset = formset_cls(instance=self.object)
            context[key] = formset
        return context


class AutoSlugMixin(object):
    """
    Automatically set slug to slugified version of the name if left empty.
    Use this as follows::

        class MyModel(AutoSlugMixin, models.Model):
            def save(self):
                super(MyModel, self).save()

                self.update_slug()

    The name of the slug field and the field to populate from can be set
    using the `_slug_from` and `_slug_field` properties.

    The big advantage of this method of setting slugs over others
    (ie. django-autoslug) is that we can set the value of slugs
    automatically based on the value of a field of an a field with a foreign
    key relation. For example::

        class MyModel(AutoSlugMixin, models.Model):
            slug = models.SlugField()

            def generate_slug(self):
                qs = self.mymodeltranslation_set.all()[:1]
                if qs.exists():
                    return qs[0].name
                else:
                    return ''

        class MyModelTranslation(models.Model):
            parent = models.ForeignKey(MyModel)
            name = models.CharField()

            def save(self):
                super(MyModel, self).save()

                self.parent.update_slug()

        (The code above is untested and _might_ be buggy.)

    """
    _slug_from = 'name'
    _slug_field = 'slug'

    def slugify(self, name):
        return slugify(name)

    def generate_slug(self):
        name = getattr(self, self._slug_from)
        return self.slugify(name)

    def update_slug(self, commit=True):
        if not getattr(self, self._slug_field) and \
               getattr(self, self._slug_from):
            setattr(self, self._slug_field, self.generate_slug())

            if commit:
                self.save()


class AutoUniqueSlugMixin(AutoSlugMixin):
    """ Make sure that the generated slug is unique. """

    def is_unique_slug(self, slug):
        qs = self.__class__.objects.filter(**{self._slug_field: slug})
        return not qs.exists()

    def generate_slug(self):
        original_slug = super(AutoUniqueSlugMixin, self).generate_slug()
        slug = original_slug

        iteration = 1
        while not self.is_unique_slug(slug):
            slug = "%s-%d" % (original_slug, iteration)
            iteration += 1

        return slug
